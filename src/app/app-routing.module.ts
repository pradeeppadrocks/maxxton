import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DistinctDepartmentsComponent } from './distinct-departments/distinct-departments.component';
import { EmployeeComponent } from './employee/employee.component';
import { ExperienceComponent } from './experience/experience.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'employee-list',
    pathMatch: 'full'
  },

  {
    path: 'employee-list',
    component: EmployeeComponent
  },

  {
    path: `experience`,
    component: ExperienceComponent
  },
  {
    path: `distinct-departments`,
    component: DistinctDepartmentsComponent
  },

  {
    path: '**',
    redirectTo: 'employee-list'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
