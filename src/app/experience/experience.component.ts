import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit {
  dataList: any;

  constructor(private router: Router) { }


  ngOnInit(): void {
    this.dataList = history.state.data
    if (this.dataList?.length>0) {
      this.dataList = this.dataList.filter((value: { exp: number; }) => value.exp > 2);
      console.log(this.dataList.filter((value: { department: string; }) => value.department === 'HR'))
    } else {
      this.goToEmployee();
    }
  }

  goToEmployee(): void {
    this.router.navigate(['/employee-list']);
  }

}
