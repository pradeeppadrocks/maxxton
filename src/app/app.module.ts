import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeeComponent } from './employee/employee.component';
import { SortDirective } from './directive/sort.directive';
import { FormsModule } from '@angular/forms';
import { ExperienceComponent } from './experience/experience.component';
import { DistinctDepartmentsComponent } from './distinct-departments/distinct-departments.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,
    SortDirective,
    ExperienceComponent,
    DistinctDepartmentsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
