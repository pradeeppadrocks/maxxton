import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DistinctDepartmentsComponent } from './distinct-departments.component';

describe('DistinctDepartmentsComponent', () => {
  let component: DistinctDepartmentsComponent;
  let fixture: ComponentFixture<DistinctDepartmentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DistinctDepartmentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DistinctDepartmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
