import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-distinct-departments',
  templateUrl: './distinct-departments.component.html',
  styleUrls: ['./distinct-departments.component.scss']
})
export class DistinctDepartmentsComponent implements OnInit {

  dataList: any;
  HrDepartments: number = 0;
  HrFinance: number = 0;
  HrOperations: number = 0;
  HrDevelopment: number = 0;


  constructor(private router: Router) { }


  ngOnInit(): void {
    this.dataList = history.state.data
    if (this.dataList?.length > 0) {
      this.dataList.forEach((element: any) => {
        console.log(element);
        if (element.department === 'HR') {
          this.HrDepartments += 1
        }
        if (element.department === 'Finance') {
          this.HrFinance += 1
        }
        if (element.department === 'Operations') {
          this.HrOperations += 1
        }
        if (element.department === 'Development') {
          this.HrDevelopment += 1
        }
      })
    } else {
      this.goToEmployee();
    }
  }

  goToEmployee(): void {
    this.router.navigate(['/employee-list']);
  }

}
