import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  dateFlag = true;
  content: any
  searchEmployee: '' = "";

  public dataList: Array<IEmployee> = [
    { id: 11, name: "Ash", department: "Finance", joining_date: '2018-08-15', exp: 1 },
    { id: 12, name: "John", department: "HR", joining_date: '2019-6-15', exp: 4 },
    { id: 13, name: "Zuri", department: "Operations", joining_date: '2018-3-25', exp: 2 },
    { id: 14, name: "Vish", department: "Development", joining_date: '2020-08-10', exp: 3 },
    { id: 15, name: "John", department: "Operations", joining_date: '2019-1-19', exp: 5 },
    { id: 16, name: "Ady", department: "Finance", joining_date: '2014-04-22', exp: 7 },
    { id: 17, name: "Gare", department: "Development", joining_date: '2018-08-15', exp: 7 },
    { id: 18, name: "Hola", department: "Development", joining_date: '2012-08-20', exp: 1 },
    { id: 19, name: "Ola", department: "Finance", joining_date: '2018-08-15', exp: 1 },
    { id: 20, name: "Kim", department: "Finance", joining_date: '2018-08-15', exp: 1 },
  ]

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.content = this.dataList
  }
  sortData(flag: boolean) {
    // return this.dataList.sort((a:any , b:any) => b.joining_date - a.joining_date)

    if (flag) {
      this.dateFlag = false;
      return this.dataList.sort((a: { joining_date: string | number | Date; }, b: { joining_date: string | number | Date; }) => {
        return <any>new Date(b.joining_date) - <any>new Date(a.joining_date);
      });
    } else {
      this.dateFlag = true;

      return this.dataList.sort((a: { joining_date: string | number | Date; }, b: { joining_date: string | number | Date; }) => {
        return <any>new Date(a.joining_date) - <any>new Date(b.joining_date);
      });
    }

  }

  search() {
    let searchTerms = this.searchEmployee
    if (this.searchEmployee) {
      this.dataList = this.dataList.filter(function (ele: { name: string; }, i: any, array: any) {
        let arrayElement = ele.name.toLowerCase()
        return arrayElement.includes(searchTerms)
      })
    }
    else {
      this.dataList = this.content
    }
  }

  goToExperience(): void {
    this.router.navigate(['/experience'], { state: { data: this.content } });
  }
  goToDepartments(): void {
    this.router.navigate(['/distinct-departments'], { state: { data: this.content } });
  }

  deleteDevelopment():void{
    this.dataList = this.dataList.filter((value: { department: string; }) => value.department != 'Development');
    this.content = this.dataList
  }


}


export interface IEmployee {
  id: number,
  name: string,
  department: string,
  joining_date: string,
  exp: number
}
